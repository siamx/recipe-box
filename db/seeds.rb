# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

User.create(name: "Hend Abied", email: "chief@hend.me", password: "12345678")


Recipe.create(
                 title: "Test",
           description: "Tesr Desc",
               user_id: 1,
       image_file_name: "test.jpeg",
    image_content_type: "image/jpeg"
)

Ingredient.create(name: "ingredient 1", recipe_id: 1)

Direction.create(step: "step 1", recipe_id: 1)


Recipe.create(
                 title: "Chicken Alfredo.",
           description: "Classic and easy to make, this version of Alfredo doesn't use flour to thicken the sauce. Instead, it relies on a slight simmer and a heavy dose of cheese.",
               user_id: 1,
       image_file_name: "chicken-alfredo.jpeg",
    image_content_type: "image/jpeg"
)

Ingredient.create(name: "Kosher salt", recipe_id: 2)
Ingredient.create(name: "12 ounces fettuccine", recipe_id: 2)
Ingredient.create(name: "Olive oil, for tossing", recipe_id: 2)
Ingredient.create(name: "12 ounces boneless, skinless chicken breast (about 2)", recipe_id: 2)
Ingredient.create(name: "Freshly ground black pepper", recipe_id: 2)
Ingredient.create(name: "1 stick (8 tablespoons) unsalted butter", recipe_id: 2)
Ingredient.create(name: "2 cups heavy cream", recipe_id: 2)
Ingredient.create(name: "2 pinches freshly grated nutmeg", recipe_id: 2)
Ingredient.create(name: "1 1/2 cups freshly grated Parmigiano-Reggiano cheese", recipe_id: 2)

Direction.create(step: "Bring a large pot of water to a boil, and salt generously. Add the pasta, and boil according to package directions until al dente, tender but still slightly firm. Strain, and toss with a splash of oil.", recipe_id: 2)
Direction.create(step: "Meanwhile, slice the chicken into 1/4-inch-thick strips, and lay them on a plate or a sheet of waxed paper. Season with salt and pepper.", recipe_id: 2)
Direction.create(step: "Heat a large skillet over medium heat, and add 2 tablespoons of the butter. When the butter melts, raise the heat to medium-high and add the chicken in 1 layer. Cook, without moving the pieces, until the underside has browned, 1 to 2 minutes. Flip the pieces, and cook until browned and fully cooked through, 2 to 3 minutes more. Transfer the chicken to a medium bowl.", recipe_id: 2)
Direction.create(step: "Reduce the heat to medium, and add the remaining 6 tablespoons butter. Scrape the bottom of the skillet with a wooden spoon to release any browned bits. When the butter has mostly melted, whisk in the cream and nutmeg and bring to a simmer, then cook for 2 minutes. Lower the heat to keep the sauce just warm.", recipe_id: 2)
Direction.create(step: "Whisk the Parmigiano-Reggiano into the sauce. Add the chicken and cooked pasta, and toss well. Season with salt and pepper. Serve hot in heated bowls.", recipe_id: 2)


Recipe.create(
                 title: "Mocha Coffee Drink.",
           description: "What happens when you really, really, want a cafe mocha but really, really, really want to stay home in your pajamas? Make your own, that's what! Whether you just have drip coffee or espresso, you can bring the cafe into your own home quicker than you could change into real pants and get out the door. So put your wallet down and start with Step 1 below.",
               user_id: 1,
       image_file_name: "mocha.jpeg",
    image_content_type: "image/jpeg"
)

Ingredient.create(name: "8 ounces freshly brewed coffee (or instant)", recipe_id: 3)
Ingredient.create(name: "1/2 cup (4 ounces) milk", recipe_id: 3)
Ingredient.create(name: "1 tablespoon (15 g) cocoa powder", recipe_id: 3)
Ingredient.create(name: "1 tablespoon (15 g) warm water or warm milk (milk makes the mocha richer and creamy)", recipe_id: 3)
Ingredient.create(name: "Sugar (optional)", recipe_id: 3)
Ingredient.create(name: "Whipped cream and cocoa (optional, for topping)", recipe_id: 3)

Direction.create(step: "Gather your ingredients.", recipe_id: 3)
Direction.create(step: "Brew as much coffee as you want. To be closer to authentic, you'll want to use double-strength, dark-roasted coffee. And you could use instant coffee if you're in a pinch, but brewed coffee is just so much better.", recipe_id: 3)
Direction.create(step: "Make a cafe-style chocolate syrup with warm water and sweetened cocoa powder.", recipe_id: 3)
Direction.create(step: "In your mug, combine the chocolate syrup with your coffee.", recipe_id: 3)
Direction.create(step: "Steam some milk or heat it on the stovetop or in the microwave.", recipe_id: 3)
Direction.create(step: "Fill your mug with heated milk.", recipe_id: 3)
Direction.create(step: "Top with whip cream, a sprinkling of cocoa powder, and enjoy!", recipe_id: 3)


Recipe.create(
                 title: "Pizza Margherita.",
           description: "Yummy pizzaaaaaaaaa",
               user_id: 1,
       image_file_name: "pizza.jpeg",
    image_content_type: "image/jpeg"
)

Ingredient.create(name: "2 tablespoons sugar", recipe_id: 4)
Ingredient.create(name: "1 tablespoon kosher salt", recipe_id: 4)
Ingredient.create(name: "1 tablespoon pure olive oil", recipe_id: 4)
Ingredient.create(name: "3/4 cup warm water", recipe_id: 4)
Ingredient.create(name: "2 cups bread flour (for bread machines)", recipe_id: 4)
Ingredient.create(name: "1 teaspoon instant yeasting 2", recipe_id: 4)
Ingredient.create(name: "2 teaspoons olive oil", recipe_id: 4)
Ingredient.create(name: "Olive oil, for the pizza crust", recipe_id: 4)
Ingredient.create(name: "Flour, for dusting the pizza peel", recipe_id: 4)
Ingredient.create(name: "1 1/2 ounces pizza sauce", recipe_id: 4)
Ingredient.create(name: "1/2 teaspoon each chopped fresh herbs such as thyme, oregano, red pepper flakes, for example", recipe_id: 4)
Ingredient.create(name: "A combination of 3 grated cheeses such as mozzarella, Monterey Jack, and provolone", recipe_id: 4)

Direction.create(step: "Place the sugar, salt, olive oil, water, 1 cup of flour, yeast, and remaining cup of flour into a standing mixer's work bowl. Using the paddle attachment, start the mixer on low and mix until the dough just comes together, forming a ball. Lube the hook attachment with cooking spray. Attach the hook to the mixer and knead for 15 minutes on medium speed.", recipe_id: 4)
Direction.create(step: "Tear off a small piece of dough and flatten into a disc. Stretch the dough until thin. Hold it up to the light and look to see if the baker's windowpane, or taut membrane, has formed. If the dough tears before it forms, knead the dough for an additional 5 to 10 minutes.", recipe_id: 4)
Direction.create(step: "Roll the pizza dough into a smooth ball on the countertop. Place into a stainless steel or glass bowl. Add 2 teaspoons of olive oil to the bowl and toss to coat. Cover with plastic wrap and refrigerate for 18 to 24 hours.", recipe_id: 4)
Direction.create(step: "Place the pizza stone or tile onto the bottom of a cold oven and turn the oven to its highest temperature, about 500 degrees F. If the oven has coils on the oven floor, place the tile onto the lowest rack of the oven. Split the pizza dough into 2 equal parts using a knife or a dough scraper. Flatten into a disk onto the countertop and then fold the dough into a ball.", recipe_id: 4)
Direction.create(step: "Wet hands barely with water and rub them onto the countertop to dampen the surface. Roll the dough on the surface until it tightens. Cover one ball with a tea towel and rest for 30 minutes.", recipe_id: 4)
Direction.create(step: "Repeat the steps with the other piece of dough. If not baking the remaining pizza immediately, spray the inside of a ziptop bag with cooking spray and place the dough ball into the bag. Refrigerate for up to 6 days.", recipe_id: 4)
Direction.create(step: "Sprinkle the flour onto the peel and place the dough onto the peel. Using your hands, form a lip around the edges of the pizza. Stretch the dough into a round disc, rotating after each stretch. Toss the dough in the air if you dare. Shake the pizza on the peel to be sure that it will slide onto the pizza stone or tile. (Dress and bake the pizza immediately for a crisp crust or rest the dough for 30 minutes if you want a chewy texture.)", recipe_id: 4)
Direction.create(step: "Brush the rim of the pizza with olive oil. Spread the pizza sauce evenly onto the pizza. Sprinkle the herbs onto the pizza and top with the cheese.", recipe_id: 4)
Direction.create(step: "Slide the pizza onto the tile and bake for 7 minutes, or until bubbly and golden brown. Rest for 3 minutes before slicing.", recipe_id: 4)
